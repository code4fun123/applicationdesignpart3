#include <qiodevice.h>
#include <qaudioformat.h>
#include <qvector.h>

class MonoInput8bit : public QIODevice
{
public:
	MonoInput8bit();
	~MonoInput8bit();

	qint64 readData(char* data, qint64 maxlen);
	qint64 writeData(const char* data, qint64 len);

	QAudioFormat getAudioFormat();
	QVector<double> vecGetData();

	void setSampleRate(unsigned int);

private:
	QAudioFormat mAudioFormat;
	QVector<double> mSamples;


};