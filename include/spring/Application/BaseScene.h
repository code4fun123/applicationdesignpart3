#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include "qcustomplot.h"
#include "MonoInput8bit.h"
#include <qaudioinput.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();

  public slots:
      void mp_BackButton();
	  void mf_PlotRandom();
	  void mf_CleanPlot();
	  void mf_StartTimer();
	  void mf_StopTimer();
	
  private:
		QWidget * centralWidget;
		QCustomPlot* mv_customPlot;
		MonoInput8bit * monoInput;
		QAudioInput* mAudioInput;
		void mp_InitPlotters();

		QTimer mv_timer;

		double mv_dRefreshRate;


	};
}
