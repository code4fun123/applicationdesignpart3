#include <spring\Application\BaseScene.h>

#include "ui_base_scene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

    //connect btn's release signal to defined slot
    QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));

		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("new title goes here"));

		//setting centralWidget
		centralWidget = ui->centralwidget;

		//Get the title from transient data
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));

		// Plot
		mv_customPlot = ui->widget; // TODO
		mp_InitPlotters();

		//
		QObject::connect(ui->startButton, SIGNAL(released()), this, SLOT(mf_StartTimer()));

		// clean
		QObject::connect(ui->stopButton, SIGNAL(released()), this, SLOT(mf_StopTimer()));

		//Hz

		mv_dRefreshRate = boost::any_cast<double>(m_TransientDataCollection["RefreshRate"]);
		mv_timer.setInterval(1000/mv_dRefreshRate); // formula T=1/f

		QObject::connect(&mv_timer, SIGNAL(timeout()), this, SLOT(mf_PlotRandom()));

		unsigned int sampleRate = boost::any_cast<unsigned int>(m_TransientDataCollection["SampleRate"]);
		monoInput = new MonoInput8bit();
		monoInput->setSampleRate(sampleRate);
	}
	void BaseScene::release()
	{
		mf_StopTimer();
		delete centralWidget;
		delete monoInput;
	}

	BaseScene::~BaseScene()
	{

	}

	void BaseScene::mf_PlotRandom()
	{
		QVector<double> values = monoInput->vecGetData();
		const int lc_NO_OF_SAMPLES = values.length();
		QVector<double> y(lc_NO_OF_SAMPLES);
		QVector<double> x(lc_NO_OF_SAMPLES);
		for (int i = 0; i < lc_NO_OF_SAMPLES; i++)
		{
			x[i] = i;
			//y[i] = rand() % 20;
		}

		mv_customPlot->graph(0)->setData(x, values);
		mv_customPlot->rescaleAxes();
		mv_customPlot->replot();
	}

	void BaseScene::mf_CleanPlot()
	{
		mv_customPlot->graph(0)->data()->clear();
		mv_customPlot->replot();
	}

	void BaseScene::mf_StartTimer()
	{
		mAudioInput = new QAudioInput(monoInput->getAudioFormat());
		monoInput->open(QIODevice::WriteOnly);
		mAudioInput->start(monoInput);
		mv_timer.start();
	}

	void BaseScene::mf_StopTimer()
	{
		mAudioInput->stop();
		monoInput->close();

		mv_timer.stop();
		mf_CleanPlot();
	}

	void BaseScene::mp_InitPlotters()
	{
		// Time plot
		mv_customPlot->setInteraction(QCP::iRangeZoom, true);
		mv_customPlot->setInteraction(QCP::iRangeDrag, true);

		mv_customPlot->addGraph();
		mv_customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);

		mv_customPlot->xAxis->setLabel("sec");
		mv_customPlot->yAxis->setLabel("V");
	}


  void BaseScene::mp_BackButton()
  {
    const std::string c_szNextSceneName = "Initial scene";
    emit SceneChange(c_szNextSceneName);
	
  }

}
