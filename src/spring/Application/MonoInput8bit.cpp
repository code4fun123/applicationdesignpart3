#include "..\Application\MonoInput8bit.h"
#include <iostream>
#include <qendian.h>

MonoInput8bit::MonoInput8bit()
{
	mAudioFormat.setChannelCount(1);
	mAudioFormat.setSampleSize(8);
	mAudioFormat.setSampleType(QAudioFormat::UnSignedInt);
	mAudioFormat.setByteOrder(QAudioFormat::BigEndian);
	mAudioFormat.setCodec("audio/pcm");
}

MonoInput8bit::~MonoInput8bit()
{

}

qint64 MonoInput8bit::readData(char * data, qint64 maxlen)
{
	Q_UNUSED(data);
	Q_UNUSED(maxlen);
	return -1;


}

qint64 MonoInput8bit::writeData(const char * data, qint64 len)
{
	
	const auto* ptr = reinterpret_cast<const unsigned char*>(data);
	mSamples.clear();
	mSamples.resize(len);
	for (qint64 i = 0; i < len ; i++)
	{
		double value = qFromBigEndian<qint8>(ptr);
		mSamples[i] = value;
		ptr += 1;
	}

	return len;

}

QAudioFormat MonoInput8bit::getAudioFormat()
{
	return this->mAudioFormat;
}

QVector<double> MonoInput8bit::vecGetData()
{
	return this->mSamples;
}

void MonoInput8bit::setSampleRate(unsigned int ac_dSampleRate)
{
	mAudioFormat.setSampleRate(ac_dSampleRate);
}
